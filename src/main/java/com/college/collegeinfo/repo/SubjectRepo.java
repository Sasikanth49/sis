package com.college.collegeinfo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.college.collegeinfo.entity.Subject;

@Repository
public interface SubjectRepo extends  JpaRepository<Subject, Integer> {

}
