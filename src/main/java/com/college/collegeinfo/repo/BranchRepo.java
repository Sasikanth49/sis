package com.college.collegeinfo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.college.collegeinfo.entity.Branch;

@Repository
public interface BranchRepo extends  JpaRepository<Branch, Integer> {

}
