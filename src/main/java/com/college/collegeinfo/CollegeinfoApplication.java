package com.college.collegeinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;


@SpringBootApplication(exclude = {
        SecurityAutoConfiguration.class}
        )


public class CollegeinfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollegeinfoApplication.class, args);
	}

}
