package com.college.collegeinfo.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Student {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	private int id;
	private String studentName;
	private String studentgender;
	private String studentaddress;
	private int studentAge;
	private int branchId;
	
	
	  @ManyToOne(fetch = FetchType.LAZY, optional = false)
	    @JoinColumn(name = "college_id", nullable = false)
	    @OnDelete(action = OnDeleteAction.CASCADE)
	    @JsonIgnore
	private College college;
	
	public int getBranchName() {
		return branchId;
	}

	public void setBranchName(int branchName) {
		this.branchId = branchId;
	}

	

	public College getCollege() {
		return college;
	}

	public void setCollege(College college) {
		this.college = college;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;

	}

	public int getStudentAge() {
		return studentAge;
	}

	public void setStudentAge(int studentAge) {
		this.studentAge = studentAge;

	}

	public String getStudentgender() {
		return studentgender;
	}

	public void setStudentgender(String studentgender) {
		this.studentgender = studentgender;
	}

	public String getStudentaddress() {
		return studentaddress;
	}

	public void setStudentaddress(String studentaddress) {
		this.studentaddress = studentaddress;
	}

}
