package com.college.collegeinfo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Teacher {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private int collegeId;
	private String teacherName;
	private String techergender;
	private String teacheraddress;
	private int teacherSalary;
	private String teacherSubject;

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCollegeId() {
		return collegeId;
	}

	public void setCollegeId(int collegeId) {
		this.collegeId = collegeId;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getTechergender() {
		return techergender;
	}

	public void setTechergender(String techergender) {
		this.techergender = techergender;
	}

	public String getTeacheraddress() {
		return teacheraddress;
	}

	public void setTeacheraddress(String teacheraddress) {
		this.teacheraddress = teacheraddress;
	}

	public int getTeacherSalary() {
		return teacherSalary;
	}

	public void setTeacherSalary(int teacherSalary) {
		this.teacherSalary = teacherSalary;
	}

	public String getTeacherSubject() {
		return teacherSubject;
	}

	public void setTeacherSubject(String teacherSubject) {
		this.teacherSubject = teacherSubject;
	}

}
